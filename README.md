﻿
## Установка

`$ npm install -g sequelize-cli`

`$ npm install`

## База данных
 По умолчанию используется учетная записть root\root.
 Нужно подключиться к базе и создать пользователя запросом, потом изменить пользователя в ./src/config/index.js

```
CREATE USER 'nodejs'@'localhost' IDENTIFIED BY '111';

GRANT ALL PRIVILEGES ON *.* TO 'nodejs'@'localhost';

FLUSH PRIVILEGES;
```


## Разработка
### Запуск
```
npm run start
```

### Миграции
#### Применить миграции
```
npm run migrate
```

## Docker
### Запуск Docker
```
docker-compose up
```

#### Применить миграции Docker
```
npm run migratedocker
```
