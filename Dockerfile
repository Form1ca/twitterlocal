FROM node:8.9-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
COPY wait-for-it.sh ./
RUN npm install --production --silent && mv node_modules ../
COPY . .
EXPOSE 10021
CMD [ "npm", "run", "startdocker" ]