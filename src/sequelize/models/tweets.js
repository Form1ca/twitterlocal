const tableName = 'tweets';


export default (sequelize, DataTypes) => {
  // const Tweets = sequelize.define(tableName,
  //   {
  //     created_at: { type: DataTypes.DATE },
  //     id: {
  //       type: DataTypes.BIGINT,
  //       primaryKey: true,
  //       autoIncrement: false,
  //     },
  //     id_str: { type: DataTypes.STRING },
  //     text: { type: DataTypes.STRING },
  //     truncated: { type: DataTypes.BOOLEAN },
  //     entities: { type: DataTypes.JSON },
  //     metadata: { type: DataTypes.JSON },
  //     source: { type: DataTypes.STRING },
  //     in_reply_to_status_id: { type: DataTypes.BIGINT },
  //     in_reply_to_status_id_str: { type: DataTypes.STRING },
  //     in_reply_to_user_id: { type: DataTypes.BIGINT },
  //     in_reply_to_user_id_str: { type: DataTypes.STRING },
  //     in_reply_to_screen_name: { type: DataTypes.STRING },
  //     user: { type: DataTypes.JSON },
  //     geo: null,
  //     coordinates: null,
  //     place: null,
  //     contributors: null,
  //     retweeted_status: { type: DataTypes.JSON },
  //     is_quote_status: { type: DataTypes.BOOLEAN },
  //     retweet_count: { type: DataTypes.INT },
  //     favorite_count: { type: DataTypes.INT },
  //     favorited: { type: DataTypes.BOOLEAN },
  //     retweeted: { type: DataTypes.BOOLEAN },
  //     lang: { type: DataTypes.STRING },
  //     originsource: { type: DataTypes.JSON },
  //     rating: { type: DataTypes.INT },
  //   });
  const Tweets = sequelize.define(tableName, {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: false,
    },
    source: { type: DataTypes.JSON },
    rating: { type: DataTypes.INTEGER },
  });

  return Tweets;
};

export {
  tableName,
};
