/* eslint-disable no-loop-func */
/* eslint-disable no-await-in-loop */


import config from '../config/config';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const basename = path.basename(__filename);
const db = {};

const sequelize = new Sequelize(config);

fs
  .readdirSync(__dirname)
  .filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.testConnection = () => {
  sequelize.authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch((err) => {
      console.error('Unable to connect to the database:', err);
    });
};


// задержка
function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

// пытаем подключиться к базе, пока не получится
db.waitConnection = async () => {
  let connect = false;
  let counter = 0;
  while (!connect) {
    counter += 1;

    await sequelize.authenticate()
      .then(() => {
        console.log('Connection has been established successfully.');
        connect = true;
      })
      .catch(async () => {
        console.error('Unable to connect to the database:', counter);
        await sleep(5000);
      });
  }
  sequelize.sync();
};

sequelize.sync();

db.sequelize = sequelize;
db.Sequelize = Sequelize;


module.exports = db;
