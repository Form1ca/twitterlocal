const tableName = require('../models/tweets');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    await queryInterface.createTable(tableName,
      {
        id: {
          type: Sequelize.BIGINT,
          primaryKey: true,
          autoIncrement: false,
        },
        source: { type: Sequelize.JSON },
        rating: { type: Sequelize.INTEGER },
        createdAt: { type: Sequelize.DATE },
        updatedAt: { type: Sequelize.DATE },
      }, {
        charset: 'utf8',
        collate: 'utf8_general_ci',
      });
  },

  down: async (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    await queryInterface.dropTable(tableName);
  },
};
