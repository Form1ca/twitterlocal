import config from '../../config';

config.sequelize.benchmark = true;
config.sequelize.operatorsAliases = false;

export default config.sequelize;
