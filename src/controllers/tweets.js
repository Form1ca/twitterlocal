import to from 'await-to-js';
import db from '../sequelize/models';


const returnStatus = (ctx, code = 200, body = '') => {
  ctx.code = code;
  ctx.body = body;
  return ctx;
};

// добавляем или переписываем 1 твит в базе
function upsert(twit) {
  return db.tweets
    .findByPk(twit.id)
    .then((obj) => {
      if (obj) { // update
        return obj.update({
          id: obj.id,
          source: twit,
          rating: obj.rating,
        });
      }
      // insert
      return db.tweets.create({
        id: twit.id,
        source: twit,
        rating: 0,
      });
    });
}

// массив твитов  добавляем в базу по одному
const updateTweets = async tweets => new Promise((resolve, reject) => {
  try {
    tweets.forEach((el) => {
      upsert(el);
    });
    resolve();
  } catch (error) {
    reject(error);
  }
});


// сохраняем массив твитов
const save = async (ctx) => {
  console.log('---save---');
  const tweet = ctx.request.body;
  if (!tweet || tweet.length === 0) {
    returnStatus(ctx, 400, 'Parameter not found.');
    return;
  }


  try {
    await updateTweets(tweet);
  } catch (error) {
    console.log('error add row', error);
    returnStatus(ctx, 500, error);
    return;
  }


  returnStatus(ctx, 200, 'OK');
};


// оцениваем
const rate = async (ctx) => {
  const id = ctx.params.id;
  const rating = ctx.params.rating;
  if (!id || !rating) {
    returnStatus(ctx, 400, 'Parameter not found.');
    return;
  }


  let res;
  try {
    const twit = await db.tweets.findByPk(id);
    if (!twit) {
      console.log('twit not found');
      returnStatus(ctx, 500, 'Twit not found');
      return;
    }

    twit.rating = rating;

    res = twit.update({ rating });
  } catch (error) {
    console.log('error change row', error);
    returnStatus(ctx, 500, error);
    return;
  }


  returnStatus(ctx, 200, res);
};

// убираем метаданные sequelize, формируем пагинатор
const parseResult = (data, filter) => {
  const pager = {
    data: [],
    count: data.count || 0,
    offset: filter.offset || 0,
    limit: filter.limit || 0,
  };
  data.rows.forEach((element) => {
    pager.data.push(element.dataValues);
  });
  return pager;
};

// получаем твиты из базы с оценками
const getSaved = async (ctx) => {
  const offset = ctx.request.query.offset;
  const limit = ctx.request.query.limit;

  const filter = {
    attributes: ['source', 'rating'],
  };

  filter.offset = (offset) ? parseInt(offset, 10) : 0;
  filter.limit = (limit) ? parseInt(limit, 10) : 20;


  const [error, data] = await to(db.tweets.findAndCountAll(filter));
  if (!data || error) {
    returnStatus(ctx, 500, error);
    return;
  }

  const parsed = parseResult(data, filter);

  returnStatus(ctx, 200, parsed);
};


export default {
  getSaved,
  save,
  rate,
};
