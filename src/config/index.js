const env = process.env;
const host = env.SERVICE_HOST || 'localhost';
const environment = env.NODE_ENV || 'development';
const serviceName = env.SERVICE_NAME || 'TwitterLocal';


const defaultConfig = {
  environment,
  serviceName,
  host,
};


const config = {
  development: {
    port: env.SERVICE_PORT || 10021,
    sequelize: {
      host: '127.0.0.1',
      port: 3306,
      database: 'tweets',
      username: 'root',
      password: 'root',
      dialect: 'mysql',
    },
  },
  test: {
    port: env.SERVICE_PORT || 10021,
    sequelize: {
      host: 'localhost',
      port: 3306,
      database: 'tweets',
      username: 'nodejs',
      password: '111',
      dialect: 'mysql',
    },
  },
  migrate: {
    port: env.SERVICE_PORT || 10021,
    sequelize: {
      host: 'localhost',
      port: 3307,
      database: 'tweets',
      username: 'root',
      password: 'root',
      dialect: 'mysql',
    },
  },
  production: {
    port: env.SERVICE_PORT || 10021,
    sequelize: {
      host: 'mysql',
      port: 3306,
      database: 'tweets',
      username: 'root',
      password: 'root',
      dialect: 'mysql',
    },
  },
};


export default { ...defaultConfig, ...config[environment] };
