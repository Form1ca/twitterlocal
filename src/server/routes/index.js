import Router from 'koa-router';
import tweets from '../../controllers/tweets';


const router = new Router();

router
  .get('/tweets/', tweets.getSaved) // get local twits with rating
  .post('/tweets/', tweets.save) // save twit
  .put('/tweets/:id/rate/:rating', tweets.rate); // set/change rating

export function routes() { return router.routes(); }
export function allowedMethods() { return router.allowedMethods(); }
